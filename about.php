<?php
    session_start();
    error_reporting(0);


?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Winecastle </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--CUSTOM CSS START-->
<link href="css/custom.css" rel="stylesheet" type="text/css">
<!--BOOTSTRAP CSS START-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<!--RESPONSIVE CSS START-->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!--COLOR CSS START-->
<link href="css/color.css" rel="stylesheet" type="text/css">
<!--FONTAWESOME CSS START-->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--OWL CAROUSEL CSS START-->
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<!--PRETTYPHOTO CSS START-->
<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css">
<script>
document.getElementById("myh4").style.color = "#ffffff";
</script>
</head>

<body>
<div id="wrapper">
  <header id="header">
    <section class="header-section-1">
      <div class="container">
        <div class="left-box"> <span>Email pentru detalii:</span> <a href="mailto:">info@winecastle.com</a> </div>
        <div class="shop-box">
        </div>
        <div class="header-social">
        <p>
        <?php
    include "database.php";
    $user = R::findOne('users', 'login = ?', [array($_SESSION['login'])]);

    if ($_SESSION['admin'] != true && $_SESSION['guest'] != true)
        header("Location: about.php");

    else { ?>
             <p class="font-italic text-center"><h4  id="myh4"> Bine ai venit:   <?php  echo $_SESSION['login']; ?></h4></p>
    <? } ?>

        </p>

        </div>
      </div>
    </section>
    <section class="header-section-2">
      <div class="container"> <a href="index.php" class="logo"><img src="images/logo.png" alt="logo"></a>
        <div class="navigation-row">
          <nav class="navbar navbar-inverse">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav" id="nav">
                <li><a href="index.php">Home</a></li>
                <li class="active"><a href="about.php">About</a></li>
                <li><a href="#">Products</a>
                  <ul>
                    <li><a href="product.php">Products</a></li>
                    <li><a href="product-detail.php">Products Detail</a></li>
                  </ul>
                </li>
                <li><a href="#">Events<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                  <ul>
                    <li><a href="event.php">Events</a></li>
                  </ul>
                </li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="#">User</a>
                  <ul>
                    <li><a class="btn wow fadeInDown"  data-wow-delay="0.4s" id="register" data-toggle="modal" data-target="#modalRegisterForm">Registration</a></li>
                    <li> <a class="btn  wow fadeInDown" data-wow-delay="0.4s" id="signIn" data-toggle="modal" data-target="#modalLogForm">Sign in</a></li>
                    <li> <a href="logout.php">Logout</a></li>
                  </ul>
                </li>
            </div>
          </nav>
        </div>
        <div class="modal myModal fade" id="modalRegisterForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content"  >
           <div class="modal-header text-center">
               <h4 class="modal-title w-100 font-weight-bold" id="signUp">Registration</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
          <form method="POST" action='registr.php'>

                <div class="modal-body mx-3">
                   <div class="md-form mb-5">
                       <i class="fas fa-user prefix grey-text"></i>
                       <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                   </div>
                   <div class="md-form mb-5">
                       <i class="fas fa-envelope prefix grey-text"></i>
                       <input type="email" name="email"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                   </div>

                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password_2"   style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Confirm password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="hidden"  name="id" class="form-control validate">
                   </div>
                </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" name="registr" class="btn btn-info">register</button>
            </div>
           </form>
       </div>
   </div>
</div>



<div class="modal fade" id="modalLogForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold" id="signUp">Log in</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
               <form method="POST" action='login_1.php'>

                <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                        </div>
                        <div class="md-form mb-4">
                            <i class="fas fa-lock prefix grey-text"></i>
                            <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                        </div>

                 </div>
                 <div class="modal-footer d-flex justify-content-center">
                     <button type="submit" name="admin" class="btn btn-info">Log in</button>
                 </div>
                </form>
            </div>
        </div>
     </div>
        <div class="sidebar-box">
          <form action="#">
            <input placeholder="Introdu text" required type="text">
            <button type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
          </form>
        </div>
      </div>
    </section>
  </header>
  <!--INNER BANNER START-->
  <div id="inner-banner">
    <div class="container">
      <h1>About Page</h1>
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">About</li>
      </ol>
    </div>
  </div>
  <!--INNER BANNER END-->
  <div id="main">
    <!--WELCOME SECTION START-->
    <section class="welcome-section">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <div class="text-box">
              <h2>Bine ai venit la WineCastle</h2>
              <p>Orice vin începe de la struguri și o vinarie adevarată este obligata sa dețina vița de vie. Din acest motiv, pe parcursul a 10 ani, noi am plantat peste 500 hectare de viță de vie.</p>
              <a href="#" class="btn-style-1">Citeste mai mult</a> </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="thumb"><img src="images/welcome-img-1.jpg" alt=""></div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="video-frame">
              <iframe src="https://www.youtube.com/embed/ANkI2RZApeI"></iframe>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--WELCOME SECTIO END-->

    <section class="gallery-section home-gallery">
      <div class="container-fluid">
        <div class="gallery">
          <div class="col-md-3 col-sm-6">
            <div class="frame"> <img src="images/gallery/gallery-col-3-img-1.jpg" alt="img"> <a href="images/gallery/gallery-col-3-img-1.jpg" class="link" data-rel="prettyPhoto[gallery1]"><i class="fa fa-image" aria-hidden="true"></i></a> </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="frame"> <img src="images/gallery/gallery-col-3-img-2.jpg" alt="img"> <a href="images/gallery/gallery-col-3-img-2.jpg" class="link" data-rel="prettyPhoto[gallery1]"><i class="fa fa-image" aria-hidden="true"></i></a> </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="frame"> <img src="images/gallery/gallery-col-3-img-3.jpg" alt="img"> <a href="images/gallery/gallery-col-3-img-3.jpg" class="link" data-rel="prettyPhoto[gallery1]"><i class="fa fa-image" aria-hidden="true"></i></a> </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="frame"> <img src="images/gallery/gallery-col-3-img-4.jpg" alt="img"> <a href="images/gallery/gallery-col-3-img-4.jpg" class="link" data-rel="prettyPhoto[gallery1]"><i class="fa fa-image" aria-hidden="true"></i></a> </div>
          </div>


        </div>
      </div>
    </section>


    <section class="parallax-section">
      <div class="container">
        <div class="holder"><strong>O casă digitală de lux pentru un Brand care a șocat lumea</strong><a href="#" class="btn-style-1">Cumpara acum</a></div>
      </div>
    </section>
    <section class="testimonial-section">
      <div class="container">
        <h2>Consumatori Fericiti</h2>
        <div class="inner-box">
          <div class="owl-controls post-slider">
            <div class="item">
              <p>Taraboste este gama de vârf de la Vartely și acest nume nu a dezamăgit până acum. Ediția din 2017 surprinde, însă, cu un stil nou. Anterior, în gama Taraboste (mă refer la vinuri albe) era îmbuteliat preponderent Chardonnay baricat. Era vorba de vinuri intense, corpolente, onctuoase. </p>
              <div class="btm-row">
                <div class="thumb"><img src="images/testimonial-img-2.jpg" alt=""></div>
                <strong class="name">Arina Melnic</strong> </div>
            </div>
            <div class="item">
              <p>Un vin-premieră în Republica Moldova. Soi de origine autohtonă, creat la Institutul Național al Viei și Vinului, în rezultatul încrucișării Villard Blanc și Risling de Rhin.</p>
              <div class="btm-row">
                <div class="thumb"><img src="images/testimonial-img-2.jpg" alt=""></div>
                <strong class="name">Scarlett Pisco</strong> </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!--FOOTER START-->
  <footer id="footer">
    <!--FOOTER SECTION 1 START-->
    <section class="footer-section-1">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="footer-box"> <strong class="footer-logo"><a href="#"><img src="images/footer-logo.png" alt="img"></a></strong>
              <p>Organizam excursii la vinarie unde puteți vedea tot procesul de vinificație. Puteți vedea cum strugurii recoltați se prelucrează și se preseaza, cum mustul este fermentati si transformat in vin și cum vinurile evolueaza după fermentare datorita unor procese ca și maturare in stejar sau în sticlă.</p>
              <a href="#" class="btn-style-1">Citeste mai mult</a> </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Alte servicii</h3>
              <div class="dental-services">
                <ul>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Micul dejun</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Masa de pranz</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Turism</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Excursie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Retele de socializare</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bar & Wine Service</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Istorie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Hotel</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Producere</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Blogul recent</h3>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-1.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Apache</a></li>
                    <li><a href="#">02 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Preturile la vin sunt greșite pentru a păcăli oamenii</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-2.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Tony Stark</a></li>
                    <li><a href="#">12 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Singurul vin moldovenesc premiat cu medalie de aur la concursul Mundus Vini 2016.</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-3.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Capitan America</a></li>
                    <li><a href="#">22 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Aciditatea bine echilibrată și cele 14.8% alcool îi oferă premise pentru o învechire de durată la sticlă. </a> </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Locatia</h3>
              <div class="footer-map">
                <div id="map_contact_2" class="map_canvas active"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--FOOTER SECTION 1 END-->

    <!--FOOTER SECTION 3 START-->
    <section class="footer-section-3">
      <div class="container"><strong class="copyright">Winecastle 2019, Toate drepturile sunt rezervate</strong></div>
    </section>
    <!--FOOTER SECTION 3 END-->
  </footer>
  <!--FOOTER END-->
</div>
<!--JQUERY START-->
<script src="js/jquery.js"></script>
<!--BOOTSTRAP JS START-->
<script src="js/bootstrap.js"></script>
<!--Map Js-->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--PRETTYPHOTO JS-->
<script src="js/jquery.prettyPhoto.js"></script>
<!--OWL CAROUSEL JS START-->
<script src="js/owl.carousel.min.js"></script>
<!--CUSTOM JS START-->
<script src="js/custom.js"></script>
</body>
</html>
