<?php
    session_start();
    error_reporting(0);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Winecastle</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--CUSTOM CSS START-->
<link href="css/custom.css" rel="stylesheet" type="text/css">
<!--BOOTSTRAP CSS START-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<!--RESPONSIVE CSS START-->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!--COLOR CSS START-->
<link href="css/color.css" rel="stylesheet" type="text/css">
<!--FONTAWESOME CSS START-->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--OWL CAROUSEL CSS START-->
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">
  <header id="header">
    <section class="header-section-1">
      <div class="container">
        <div class="left-box"> <span>Email pentru detalii:</span> <a href="mailto:">info@winecastle.com</a> </div>
        <div class="header-social">
        <?php
    include "database.php";
    $user = R::findOne('users', 'login = ?', [array($_SESSION['login'])]);

    if ($_SESSION['admin'] != true && $_SESSION['guest'] != true)
        header("Location: event.php");

    else { ?>
             <p class="font-italic text-center"><h4 style="white"> Bine ai venit:   <?php  echo $_SESSION['login']; ?></h4></p>
    <? } ?>

        </p>

        </div>
      </div>
    </section>
    <section class="header-section-2">
      <div class="container"> <a href="index.php" class="logo"><img src="images/logo.png" alt="logo"></a>
        <div class="navigation-row">
          <nav class="navbar navbar-inverse">
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav" id="nav">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Products</a>
                  <ul>
                    <li><a href="product.php">Products</a></li>
                    <li><a href="product-detail.php">Products Detail</a></li>
                  </ul>
                </li>
                <li class="active"><a href="#">Events<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                  <ul>
                    <li><a href="event.php">Events</a></li>
                  </ul>
                </li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="#">User</a>
                  <ul>
                    <li><a class="btn wow fadeInDown"  data-wow-delay="0.4s" id="register" data-toggle="modal" data-target="#modalRegisterForm">Registration</a></li>
                    <li> <a class="btn  wow fadeInDown" data-wow-delay="0.4s" id="signIn" data-toggle="modal" data-target="#modalLogForm">Sign in</a></li>
                    <li> <a href="logout.php">Logout</a></li>
                  </ul>
                </li>
            </div>
          </nav>
        </div>
        <div class="modal myModal fade" id="modalRegisterForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content"  >
           <div class="modal-header text-center">
               <h4 class="modal-title w-100 font-weight-bold" id="signUp">Registration</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
          <form method="POST" action='registr.php'>

                <div class="modal-body mx-3">
                   <div class="md-form mb-5">
                       <i class="fas fa-user prefix grey-text"></i>
                       <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                   </div>
                   <div class="md-form mb-5">
                       <i class="fas fa-envelope prefix grey-text"></i>
                       <input type="email" name="email"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                   </div>

                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password_2"   style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Confirm password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="hidden"  name="id" class="form-control validate">
                   </div>
                </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" name="registr" class="btn btn-info">register</button>
            </div>
           </form>
       </div>
   </div>
</div>



<div class="modal fade" id="modalLogForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold" id="signUp">Log in</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
               <form method="POST" action='login_1.php'>

                <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                        </div>
                        <div class="md-form mb-4">
                            <i class="fas fa-lock prefix grey-text"></i>
                            <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                        </div>

                 </div>
                 <div class="modal-footer d-flex justify-content-center">
                     <button type="submit" name="admin" class="btn btn-info">Log in</button>
                 </div>
                </form>
            </div>
        </div>
     </div>
        <div class="sidebar-box">
          <form action="#">
            <input placeholder="Introdu text" required type="text">
            <button type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
          </form>
        </div>
      </div>
    </section>
  </header>
  <!--INNER BANNER START-->
  <div id="inner-banner">
    <div class="container">
      <h1>Events</h1>
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">Events</li>
      </ol>
    </div>
  </div>
  <!--INNER BANNER END-->
  <div id="main">
    <!--BLOG START-->
    <section class="blog-section">
      <div class="container">
        <div class="row">
          <div class="col-md-9 col-sm-8">
            <!--EVENT POST START-->
            <div class="post-box">
              <div class="frame"> <a href="#"><img src="images/blog/blog-img-4.jpg" alt="img"></a>
                <div class="countdown-box">
                  <div class="countdown236"></div>
                </div>
              </div>
              <div class="text-box">
                <h2><a href="#">America Red Wine Noir premiat ca cel mai bun din  2017</a></h2>
                <div class="post-meta">
                  <ul>
                    <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>14 Februarie, 2019</a></li>
                    <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Aria Stark</a></li>
                    <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Michle</a></li>
                    <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i>195 Likes</a></li>
                  </ul>
                </div>
                <p>Este un vin ce a devenit emblematic pentru Winecastle. De o culoare rubinie închisă, vinul deschide o nouă perspectivă a simțurilor prin arome complexe de prune uscate și condimente. Gustul unic de afine și ciocolată definesc măiestria oenologului de a realiza un produs exclusiv. Vinul este maturat în butoi francez, având un bun potențial de învechire, conturând treptat noi orizonturi senzoriale.</p>
                <a href="#" class="read-post">Citeste despre</a> </div>
            </div>
            <!--EVENT POST END-->

            <!--EVENT POST START-->
            <div class="post-box">
              <div class="frame"> <a href="#"><img src="images/blog/blog-img-3.jpg" alt="img"></a>
                <div class="countdown-box">
                  <div class="countdown236"></div>
                </div>
              </div>
              <div class="text-box">
                <h2><a href="#"> America Red Wine Noir premiat ca cel mai bun din  2017</a></h2>
                <div class="post-meta">
                  <ul>
                    <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>15 Ianuarie, 2019</a></li>
                    <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>John Sins</a></li>
                    <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Bart</a></li>
                    <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i>259 Likes</a></li>
                  </ul>
                </div>
                <p>Este un vin ce a devenit emblematic pentru Winecastle. De o culoare rubinie închisă, vinul deschide o nouă perspectivă a simțurilor prin arome complexe de prune uscate și condimente. Gustul unic de afine și ciocolată definesc măiestria oenologului de a realiza un produs exclusiv. Vinul este maturat în butoi francez, având un bun potențial de învechire, conturând treptat noi orizonturi senzoriale.</p>
                <a href="#" class="read-post">Citeste despre</a> </div>
            </div>
            <!--EVENT POST END-->

            <!--EVENT POST START-->
            <div class="post-box">
              <div class="frame"> <a href="#"><img src="images/blog/blog-img-2.jpg" alt="img"></a>
                <div class="countdown-box">
                  <div class="countdown236"></div>
                </div>
              </div>
              <div class="text-box">
                <h2><a href="#">America Red Wine Noir premiat ca cel mai bun din  2017</a></h2>
                <div class="post-meta">
                  <ul>
                    <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>03 Aprilie, 2019</a></li>
                    <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>John Snow</a></li>
                    <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Michael</a></li>
                    <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i>204 Likes</a></li>
                  </ul>
                </div>
                <p>Este un vin ce a devenit emblematic pentru Winecastle. De o culoare rubinie închisă, vinul deschide o nouă perspectivă a simțurilor prin arome complexe de prune uscate și condimente. Gustul unic de afine și ciocolată definesc măiestria oenologului de a realiza un produs exclusiv. Vinul este maturat în butoi francez, având un bun potențial de învechire, conturând treptat noi orizonturi senzoriale.</p>
                <a href="#" class="read-post">Citeste despre</a> </div>
            </div>
            <!--EVENT POST END-->

            <!--EVENT POST START-->
            <div class="post-box">
              <div class="frame"> <a href="#"><img src="images/blog/blog-img-1.jpg" alt="img"></a>
                <div class="countdown-box">
                  <div class="countdown236"></div>
                </div>
              </div>
              <div class="text-box">
                <h2><a href="#"> America Red Wine Noir premiat ca cel mai bun din  2017</a></h2>
                <div class="post-meta">
                  <ul>
                    <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>14 Mai, 2019</a></li>
                    <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Margo Roby</a></li>
                    <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Carl</a></li>
                    <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i>94 Likes</a></li>
                  </ul>
                </div>
                <p>Este un vin ce a devenit emblematic pentru Winecastle. De o culoare rubinie închisă, vinul deschide o nouă perspectivă a simțurilor prin arome complexe de prune uscate și condimente. Gustul unic de afine și ciocolată definesc măiestria oenologului de a realiza un produs exclusiv. Vinul este maturat în butoi francez, având un bun potențial de învechire, conturând treptat noi orizonturi senzoriale.</p>
                <a href="#" class="read-post">Citeste despre</a> </div>
            </div>
            <!--EVENT POST END-->

            <!--PAGINATION START-->
            <div class="pagination-col">
              <nav aria-label="Page navigation">
                <ul class="pagination">
                  <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">Prev</span> </a> </li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li> <a href="#" aria-label="Next"> <span aria-hidden="true">Next</span> </a> </li>
                </ul>
              </nav>
            </div>
            <!--PAGINATION END-->
          </div>
          <div class="col-md-3 col-sm-4">
            <aside>
              <div class="sidebar">
                <div class="sidebar-box">
                  <h3>Cautare</h3>
                  <form action="#">
                    <input placeholder="Introdu textul" required type="text">
                    <button type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
                  </form>
                </div>
                <div class="sidebar-box">
                  <div class="sidebar-text-widget">
                    <h3>Despre Magazinul De Vinuri</h3>
                    <p>O vinotecă care oferă o selecție excepțională de vinuri distinse din Moldova, dar și din alte părți ale lumii. </p>
                  </div>
                </div>
                  <h3>Tags</h3>
                  <div class="tags"> <a href="#">Wine</a> <a href="#">Red Wine</a> <a href="#">White Wine</a> <a href="#">Wine Theme</a> <a href="#">Wine Shop</a> <a href="#">Wine Cafe</a> </div>
                </div>
                <div class="sidebar-box">
                  <div class="instagram-widget">
                    <ul>
                      <li><a href="#"><img src="images/instagram-img-1.jpg" alt=""></a></li>
                      <li><a href="#"><img src="images/instagram-img-2.jpg" alt=""></a></li>
                      <li><a href="#"><img src="images/instagram-img-3.jpg" alt=""></a></li>
                      <li><a href="#"><img src="images/instagram-img-4.jpg" alt=""></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>
    <!--BLOG END-->
  </div>
  <!--FOOTER START-->
  <footer id="footer">
    <!--FOOTER SECTION 1 START-->
    <section class="footer-section-1">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="footer-box"> <strong class="footer-logo"><a href="#"><img src="images/footer-logo.png" alt="img"></a></strong>
              <p>Organizam excursii la vinarie unde puteți vedea tot procesul de vinificație. Puteți vedea cum strugurii recoltați se prelucrează și se preseaza, cum mustul este fermentati si transformat in vin și cum vinurile evolueaza după fermentare datorita unor procese ca și maturare in stejar sau în sticlă.</p>
              <a href="#" class="btn-style-1">Citeste mai mult despre</a> </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Alte servicii</h3>
              <div class="dental-services">
                <ul>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Micul dejun</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Masa de pranz</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Turism</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Excursie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Retele de socializare</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bar & Wine Service</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Istorie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Hotel</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Producere</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Blogul recent</h3>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-1.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Apache</a></li>
                    <li><a href="#">02 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Preturile la vin sunt greșite pentru a păcăli oamenii</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-2.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Tony Stark</a></li>
                    <li><a href="#">12 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Singurul vin moldovenesc premiat cu medalie de aur la concursul Mundus Vini 2016.</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-3.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Capitan America</a></li>
                    <li><a href="#">22 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Aciditatea bine echilibrată și cele 14.8% alcool îi oferă premise pentru o învechire de durată la sticlă. </a> </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Locatia</h3>
              <div class="footer-map">
                <div id="map_contact_2" class="map_canvas active"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--FOOTER SECTION 1 END-->

    <!--FOOTER SECTION 3 START-->
    <section class="footer-section-3">
      <div class="container"><strong class="copyright">Winecastle 2019, Toate drepturile sunt rezervate</strong></div>
    </section>
    <!--FOOTER SECTION 3 END-->
  </footer>
  <!--FOOTER END-->
</div>
<!--JQUERY START-->
<script src="js/jquery.js"></script>
<!--BOOTSTRAP JS-->
<script src="js/bootstrap.js"></script>
<!--Map Js-->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--OWL CAROUSEL JS-->
<script src="js/owl.carousel.min.js"></script>
<!--EVENT TIMER JS-->
<script src="js/jquery_countdown.js"></script>
<!--CUSTOM JS-->
<script src="js/custom.js"></script>
</body>
</html>
