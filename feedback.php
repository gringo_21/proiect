<?php

include "database.php";

$data = $_POST;

//если кликнули на button
if ( isset($data['feedback']) )
{
    // проверка формы на пустоту полей
    $errors = array();
    if ( trim($data['name']) == '' )
    {
        $errors[] = 'enter your name';
    }

    if ( trim($data['email']) == '' )
    {
        $errors[] = 'enter Email';
    }

    if ( $data['number'] == '' )
    {
        $errors[] = 'enter number';
    }
    if ( $data['select'] == '' )
    {
        $errors[] = 'select your choose';
    }
    if ( $data['comentariu'] == '' )
    {
        $errors[] = 'enter comment';
    }


    if ( empty($errors) )
    {
        //ошибок нет, теперь регистрируем
        $user = R::dispense('feedback');
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->number = $data['number'];
        $user->vin = $data['select'];
        $user->comentariu = $data['comentariu'];


        R::store($user);
        header("Location: index.php");
        exit;
    }else
    {
        echo '<div id="errors" style="color:#ff3905;">' .array_shift($errors). '</div><hr>';
    }

}


?>

