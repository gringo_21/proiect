<?php
	session_start();
	require 'database.php';
	session_destroy();
	header('Location: index.php');
	exit();
?>
