<?php

include "database.php";

$data = $_POST;

//если кликнули на button
if ( isset($data['registr']) )
{
    // проверка формы на пустоту полей
    $errors = array();
    if ( trim($data['login']) == '' )
    {
        $errors[] = 'enter login';
    }

    if ( trim($data['email']) == '' )
    {
        $errors[] = 'enter Email';
    }

    if ( $data['password'] == '' )
    {
        $errors[] = 'enter password';
    }

    if ( $data['password_2'] != $data['password'] )
    {
        $errors[] = 'repeat passwordd!';
    }

    //проверка на существование одинакового логина
    if ( R::count('users', "login = ?", array($data['login'])) > 0)
    {
        $errors[] = 'utilizator cu asa login exista!';
    }

    //проверка на существование одинакового email
    if ( R::count('users', "email = ?", array($data['email'])) > 0)
    {
        $errors[] = 'utilizator cu asa Email exista!';
    }

    if ( empty($errors) )
    {
        //ошибок нет, теперь регистрируем
        $user = R::dispense('users');
        $user->login = $data['login'];
        $user->email = $data['email'];
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT); //пароль нельзя хранить в открытом виде, мы его шифруем при помощи функции password_hash для php > 5.6
        R::store($user);
        header("Location: index.php");
        exit;
    }else
    {
        echo '<div id="errors" style="color:#ff3905;">' .array_shift($errors). '</div><hr>';
    }

}


?>


