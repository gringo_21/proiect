<?php
    session_start();
    error_reporting(0);


?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Winecastle</title>
<link href="css/custom.css" rel="stylesheet" type="text/css">
<!--BOOTSTRAP CSS START-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<!--RESPONSIVE CSS START-->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!--COLOR CSS START-->
<link href="css/color.css" rel="stylesheet" type="text/css">
<!--FONTAWESOME CSS START-->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--OWL CAROUSEL CSS START-->
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<!--Bxslider Css-->
<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
</head>

<body>
<!--WRAPPER START-->
<div id="wrapper">
  <header id="header">
    <section class="header-section-1">
      <div class="container">
        <div class="left-box"> <span>Email detalii:</span> <a href="mailto:">info@winecastle.com</a> </div>
        <div class="header-social">
        <p>
        <?php
    include "database.php";
    $user = R::findOne('users', 'login = ?', [array($_SESSION['login'])]);

    if ($_SESSION['admin'] != true && $_SESSION['guest'] != true)
        header("Location: product-detail.php");

    else { ?>
             <p class="font-italic text-center"><h4 style="white"> Bine ai venit:   <?php  echo $_SESSION['login']; ?></h4></p>
    <? } ?>

        </p>

        </div>
      </div>
    </section>
    <section class="header-section-2">
      <div class="container"> <a href="index.php" class="logo"><img src="images/logo.png" alt="logo"></a>
        <div class="navigation-row">
          <nav class="navbar navbar-inverse">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav" id="nav">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li class="active"><a href="#">Products</a>
                  <ul>
                    <li><a href="product.php">Products</a></li>
                    <li><a href="product-detail.php">Products Detail</a></li>
                  </ul>
                </li>
                <li><a href="#">Events<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                  <ul>
                    <li><a href="event.php">Events</a></li>
                  </ul>
                </li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="#">User</a>
                  <ul>
                    <li><a class="btn wow fadeInDown"  data-wow-delay="0.4s" id="register" data-toggle="modal" data-target="#modalRegisterForm">Registration</a></li>
                    <li> <a class="btn  wow fadeInDown" data-wow-delay="0.4s" id="signIn" data-toggle="modal" data-target="#modalLogForm">Sign in</a></li>
                    <li> <a href="logout.php">Logout</a></li>
                  </ul>
                </li>
            </div>
          </nav>
        </div>
        <div class="modal myModal fade" id="modalRegisterForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content"  >
           <div class="modal-header text-center">
               <h4 class="modal-title w-100 font-weight-bold" id="signUp">Registration</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
          <form method="POST" action='registr.php'>

                <div class="modal-body mx-3">
                   <div class="md-form mb-5">
                       <i class="fas fa-user prefix grey-text"></i>
                       <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                   </div>
                   <div class="md-form mb-5">
                       <i class="fas fa-envelope prefix grey-text"></i>
                       <input type="email" name="email"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                   </div>

                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password_2"   style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Confirm password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="hidden"  name="id" class="form-control validate">
                   </div>
                </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" name="registr" class="btn btn-info">register</button>
            </div>
           </form>
       </div>
   </div>
</div>



<div class="modal fade" id="modalLogForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold" id="signUp">Log in</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
               <form method="POST" action='login_1.php'>

                <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                        </div>
                        <div class="md-form mb-4">
                            <i class="fas fa-lock prefix grey-text"></i>
                            <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                        </div>

                 </div>
                 <div class="modal-footer d-flex justify-content-center">
                     <button type="submit" name="admin" class="btn btn-info">Log in</button>
                 </div>
                </form>
            </div>
        </div>
     </div>
        <div class="sidebar-box">
          <form action="#">
            <input placeholder="Introdu text" required type="text">
            <button type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
          </form>
        </div>
      </div>
    </section>
  </header>
  <!--INNER BANNER START-->
  <div id="inner-banner">
    <div class="container">
      <h1>Product Detail</h1>
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li class="active">Product Detail</li>
      </ol>
    </div>
  </div>
  <!--INNER BANNER END-->
  <!--MAIN START-->
  <div id="main">
    <!--PRODUCT DETAIL START-->
    <section class="product-detail">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <ul class="bxslider gallery">
              <li><img src="images/product/product-img-big-1.jpg" alt="img"></li>
              <li><img src="images/product/product-img-big-2.jpg" alt="img"></li>
              <li><img src="images/product/product-img-big-3.jpg" alt="img"></li>
            </ul>
            <div id="bx-pager"> <a data-slide-index="0" href=""><img src="images/product/product-small-img-1.jpg" alt="img" /></a> <a data-slide-index="1" href=""><img src="images/product/product-small-img-2.jpg" alt="img" /></a> <a data-slide-index="2" href=""><img src="images/product/product-small-img-3.jpg" alt="img" /></a> </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="text-box">
              <h3><a href="#">America Red Wine Noir premiat ca cel mai bun din  2017</a></h3>
              <div class="btn-row"> <span class="reviews">108 recenzii clienți</span>
                <ul class="product-rating">
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
                </ul>
              </div>
              <div class="btn-row"> <span class="cut-price">$600</span> <span class="price">$359</span> </div>
              <p>Este un vin ce a devenit emblematic pentru Winecastle. De o culoare rubinie închisă, vinul deschide o nouă perspectivă a simțurilor prin arome complexe de prune uscate și condimente. Gustul unic de afine și ciocolată definesc măiestria oenologului de a realiza un produs exclusiv. Vinul este maturat în butoi francez, având un bun potențial de învechire, conturând treptat noi orizonturi senzoriale. </p>
              <div class="detail-row">
                <ul>
                  <li> Client: <b>Winecastle</b> </li>
                  <li> Design: <b>Victor</b> </li>
                  <li> Misiune: <b> wine store</b> </li>
                  <li> Data: <b> Duminica, 12 Martie, 2019</b> </li>
                </ul>
              </div>
              <div class="product-quantity">
                <div class="input-append spinner" data-trigger="spinner"> <span class="spin-down" data-spin="down"><i class="fa fa-minus"></i></span>
                  <input type="text" value="0" data-rule="quantity">
                  <span class="spin-up" data-spin="up"><i class="fa fa-plus"></i></span> </div>
              </div>
              <a href="#" class="btn-style-1">Adaugare in cos</a> </div>
          </div>
        </div>
        <div class="reviews-tab-col">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Descriere</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Recenzii</a></li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
              <div class="text-box">
                <p>APOGEU - Punctul cel mai înalt în traiectoria unui corp ceresc! Ediția limitată care te aduce mai aproape de amintirile în care priveai fascinat cerul și numărai stelele de pe el. Pe atunci  nu aveai niciodată un număr precis, noi aducem azi un număr fix pentru tine - 4444 de sticle, de corpuri cerești, care te cuceresc prin stările înălțătoare pe care le căutai. Noblețea vinului marca Vinăria din Vale, coboară către tine negru, rar și puternic, ca un cer înstelat de noapte. Baza e creată din două cupaje de excepție dintre soiurile autohtone și cele internaționale, menite să îți răsplătească fidelitatea prin arome generoase și demna realitate a primului gust.</p>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
              <div class="text-box">
                <div class="thumb"><img src="images/comment-img.jpg" alt="img"></div>
                <div class="text-col">
                  <h4>Mihai Mariana</h4>
                  <div class="btn-row"> <span class="date">Monday 27 Aprilie, 2019</span>
                    <ul class="reviews-rating">
                      <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
                    </ul>
                  </div>
                  <p>Un vin cu arome fine de fucte de pădure, cu accente de coacăze și cireșe negre. Gustul este corpolent, profund, cu note de fructe confiate, vanilie și tanini delicați.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <section class="product-section">
          <div class="container">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="product-box">
                  <div class="frame"><a href="#"><img src="images/product/product-img-1.jpg" alt=""></a></div>
                  <div class="text-box">
                    <h5><a href="#">Vin Rosu</a></h5>
                    <strong class="price">$45.00</strong>
                    <div class="cart-row">
                      <div class="cart-box"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></div>
                      <div class="like-box"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                      <div class="checkout-box"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="product-box">
                  <div class="frame"><a href="#"><img src="images/product/product-img-2.jpg" alt=""></a></div>
                  <div class="text-box">
                    <h5><a href="#">Vin Alb</a></h5>
                    <strong class="price">$99.00</strong>
                    <div class="cart-row">
                      <div class="cart-box"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></div>
                      <div class="like-box"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                      <div class="checkout-box"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="product-box">
                  <div class="frame"><a href="#"><img src="images/product/product-img-3.jpg" alt=""></a></div>
                  <div class="text-box">
                    <h5><a href="#">Vin Rosu</a></h5>
                    <strong class="price">$45.00</strong>
                    <div class="cart-row">
                      <div class="cart-box"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></div>
                      <div class="like-box"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                      <div class="checkout-box"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="product-box">
                  <div class="frame"><a href="#"><img src="images/product/product-img-4.jpg" alt=""></a></div>
                  <div class="text-box">
                    <h5><a href="#">Vin Alb</a></h5>
                    <strong class="price">$99.00</strong>
                    <div class="cart-row">
                      <div class="cart-box"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></div>
                      <div class="like-box"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                      <div class="checkout-box"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>
    <!--PRODUCT DETAIL END-->
  </div>
  <!--MAIN END-->
  <!--FOOTER START-->
  <footer id="footer">
    <!--FOOTER SECTION 1 START-->
    <section class="footer-section-1">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="footer-box"> <strong class="footer-logo"><a href="#"><img src="images/footer-logo.png" alt="img"></a></strong>
              <p>Organizam excursii la vinarie unde puteți vedea tot procesul de vinificație. Puteți vedea cum strugurii recoltați se prelucrează și se preseaza, cum mustul este fermentati si transformat in vin și cum vinurile evolueaza după fermentare datorita unor procese ca și maturare in stejar sau în sticlă.</p>
              <a href="#" class="btn-style-1">Citeste mai mult</a> </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Alte servicii</h3>
              <div class="dental-services">
                <ul>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Micul dejun</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Masa de pranz</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Turism</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Excursie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Retele de socializare</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bar & Wine Service</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Istorie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Hotel</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Producere</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Blogul recent</h3>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-1.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Apache</a></li>
                    <li><a href="#">02 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Preturile la vin sunt greșite pentru a păcăli oamenii</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-2.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Tony Stark</a></li>
                    <li><a href="#">12 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Singurul vin moldovenesc premiat cu medalie de aur la concursul Mundus Vini 2016.</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-3.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Capitan America</a></li>
                    <li><a href="#">22 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Aciditatea bine echilibrată și cele 14.8% alcool îi oferă premise pentru o învechire de durată la sticlă. </a> </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Locatia</h3>
              <div class="footer-map">
                <div id="map_contact_2" class="map_canvas active"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--FOOTER SECTION 1 END-->

    <!--FOOTER SECTION 3 START-->
    <section class="footer-section-3">
      <div class="container"><strong class="copyright">Winecastle 2019, Toate drepturile sunt rezervate</strong></div>
    </section>
    <!--FOOTER SECTION 3 END-->
  </footer>
  <!--FOOTER END-->
</div>
<!--WRAPPER END-->

<!--jQuery min-->
<script src="js/jquery.js"></script>
<!--BOOTSTRAP JS START-->
<script src="js/bootstrap.js"></script>
<!--Map Js-->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--Bxslider JS-->
<script src="js/jquery.bxslider.min.js"></script>
<!--PRETTYPHOTO JS-->
<script src="js/jquery.prettyPhoto.js"></script>
<!--PRODUCT Quantity-->
<script src="js/jquery.spinner.js"></script>
<!--CUSTOM JS START-->
<script src="js/custom.js"></script>
</body>
</html>
