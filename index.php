<?php
    session_start();
    error_reporting(0);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Winecastle</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--CUSTOM CSS START-->
<link href="css/custom.css" rel="stylesheet" type="text/css">
<!--BOOTSTRAP CSS START-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<!--RESPONSIVE CSS START-->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!--COLOR CSS START-->
<link href="css/color.css" rel="stylesheet" type="text/css">
<!--FONTAWESOME CSS START-->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--OWL CAROUSEL CSS START-->
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="style.css">
<script>
document.getElementById("myh4").style.color = "#ffffff";
</script>

</head>



<body>
<div id="wrapper">
  <header id="header">
    <section class="header-section-1">
      <div class="container">
        <div class="left-box"> <span>Email pentru detalii:</span> <a href="mailto:">info@winecastle.com</a> </div>
        <div class="shop-box">
        </div>
        <div class="header-social">
        <div>
        <?php
    include "database.php";
    $user = R::findOne('users', 'login = ?', [array($_SESSION['login'])]);

    if ($_SESSION['admin'] != true && $_SESSION['guest'] != true)
        header("Location: index.php");

    else { ?>
             <p class="font-italic text-center"><h4 id="myh4"> Bine ai venit:   <?php  echo $_SESSION['login']; ?></h4></p>
    <? } ?>
        </p>


    </div>
        </div>
      </div>
    </section>
    <section class="header-section-2">
      <div class="container"> <a href="index.php" class="logo"><img src="images/logo.png" alt="logo"></a>
        <div class="navigation-row">
          <nav class="navbar navbar-inverse">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav" id="nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Products</a>
                  <ul>
                    <li><a href="product.php">Products</a></li>
                    <li><a href="product-detail.php">Products Detail</a></li>
                  </ul>
                </li>
                <li><a href="#">Events<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                  <ul>
                    <li><a href="event.php">Events</a></li>
                  </ul>
                </li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="#">User</a>
                  <ul>
                    <li><a class="btn wow fadeInDown"  data-wow-delay="0.4s" id="register" data-toggle="modal" data-target="#modalRegisterForm">Registration</a></li>
                    <li> <a class="btn  wow fadeInDown" data-wow-delay="0.4s" id="signIn" data-toggle="modal" data-target="#modalLogForm">Sign in</a></li>
                    <li> <a href="logout.php">Logout</a></li>
                  </ul>
                </li>
            </div>

  <div class="modal myModal fade" id="modalRegisterForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content"  >
           <div class="modal-header text-center">
               <h4 class="modal-title w-100 font-weight-bold" id="signUp">Registration</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
          <form method="POST" action='registr.php'>

                <div class="modal-body mx-3">
                   <div class="md-form mb-5">
                       <i class="fas fa-user prefix grey-text"></i>
                       <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-name">New name</label>
                   </div>
                   <div class="md-form mb-5">
                       <i class="fas fa-envelope prefix grey-text"></i>
                       <input type="email" name="email"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-email">New email</label>
                   </div>

                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="password"  name="password_2"   style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                       <label data-error="wrong" data-success="right" for="orangeForm-pass">Confirm password</label>
                   </div>
                   <div class="md-form mb-4">
                       <i class="fas fa-lock prefix grey-text"></i>
                       <input type="hidden"  name="id" class="form-control validate">
                   </div>
                </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" name="registr" class="btn btn-info">register</button>
            </div>
           </form>
       </div>
   </div>
</div>



<div class="modal fade" id="modalLogForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold" id="signUp">Log in</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
               <form method="POST" action='login_1.php'>

                <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            <i class="fas fa-user prefix grey-text"></i>
                            <input type="text"  name="login"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                        </div>
                        <div class="md-form mb-4">
                            <i class="fas fa-lock prefix grey-text"></i>
                            <input type="password"  name="password"  style="background:rgba(248, 247, 247, 0.5)" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                        </div>

                 </div>
                 <div class="modal-footer d-flex justify-content-center">
                     <button type="submit" name="admin" class="btn btn-info">Log in</button>
                 </div>
                </form>
            </div>
        </div>
     </div>

          </nav>
        </div>
        <div class="sidebar-box">
          <form action="#">
            <input placeholder="Introdu text" required type="text">
            <button type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
          </form>
        </div>
      </div>
    </section>
  </header>
  <!--INNER BANNER START-->
  <div id="banner">
    <div class="owl-carousel" id="home-slider">s
      <div class="item"> <img src="images/banner-img-2.jpg" alt="">
        <div class="caption">
          <div class="container">
            <div class="inner-box">
              <h1>Misiunea noastra este de a face vin</h1>
              <strong>DEZVĂLUIND NUANȚELE COMPLEXE ALE OCEANULUI-INFLUENȚATE, PODGORII MONTANE CARE LE INSPIRA.</strong> <a href="#" class="btn-style-1">Cumpara acum</a> <a href="#" class="btn-style-1">Contacteaza-ne</a> </div>
          </div>
        </div>
      </div>
      <div class="item"> <img src="images/banner-img-1.jpg" alt="">
        <div class="caption">
          <div class="container">
            <div class="inner-box">
              <h1>O casă digitală de lux pentru un Brand care a șocat lumea</h1>
              <strong>DEZVĂLUIND NUANȚELE COMPLEXE ALE OCEANULUI-INFLUENȚATE, PODGORII MONTANE CARE LE INSPIRA.</strong> <a href="#" class="btn-style-1">Cumpara acum</a> <a href="#" class="btn-style-1">Contacteaza-ne</a> </div>
          </div>
        </div>
      </div>
      <div class="item"> <img src="images/banner-img-3.jpg" alt="">
        <div class="caption">
          <div class="container">
            <div class="inner-box">
              <h1>O casă digitală de lux pentru un Brand care a șocat lumea</h1>
              <strong>DEZVĂLUIND NUANȚELE COMPLEXE ALE OCEANULUI-INFLUENȚATE, PODGORII MONTANE CARE LE INSPIRA.</strong> <a href="#" class="btn-style-1">Cumpara acum</a> <a href="#" class="btn-style-1">Contacteaza-ne</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--INNER BANNER END-->
  <div id="main">
    <!--WELCOME SECTION START-->
    <section class="welcome-section">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <div class="text-box">
              <h2>Bine ai venit la WineCastle</h2>
              <p>Orice vin începe de la struguri și o vinarie adevarată este obligata sa dețina vița de vie. Din acest motiv, pe parcursul a 10 ani, noi am plantat peste 500 hectare de viță de vie.</p>
              <a href="#" class="btn-style-1">Citeste mai mult</a> </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="thumb"><img src="images/welcome-img-1.jpg" alt=""></div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="video-frame">
              <iframe src="https://www.youtube.com/embed/ANkI2RZApeI"></iframe>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--WELCOME SECTIO END-->

    <!--ONLINE PRODUCT SECTION START-->
    <section class="online-product">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6">
            <div class="product-col">
              <div class="thumb"><a href="#"><img src="images/shop-product-img-1.jpg" alt=""></a><i class="fa fa-beer" aria-hidden="true"></i></div>
              <div class="text-col">
                <h3><a href="#">Red Wine</a></h3>
                <ul class="rating">
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star-half-full" aria-hidden="true"></i></a></li>
                </ul>
                <p>Vinul roșu este un tip de vin fabricat din soiuri de struguri de culoare închisă (negru).</p>
                <strong class="price">$12.00</strong> </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="product-col">
              <div class="thumb"><a href="#"><img src="images/shop-product-img-2.jpg" alt=""></a><i class="fa fa-beer" aria-hidden="true"></i></div>
              <div class="text-col">
                <h3><a href="#">Vin Alb</a></h3>
                <ul class="rating">
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star-half-full" aria-hidden="true"></i></a></li>
                </ul>
                <p>Vinul alb este un vin care este fermentat fără contact cu coarja boabelor de poama.</p>
                <strong class="price">$18.0</strong> </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="product-col">
              <div class="thumb"><a href="#"><img src="images/shop-product-img-3.jpg" alt=""></a><i class="fa fa-beer" aria-hidden="true"></i></div>
              <div class="text-col">
                <h3><a href="#">Vin Rosu</a></h3>
                <ul class="rating">
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star-half-full" aria-hidden="true"></i></a></li>
                </ul>
                <p>Vinul roșu este un tip de vin fabricat din soiuri de struguri de culoare închisă (negru).</p>
                <strong class="price">$66.00</strong> </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="product-col">
              <div class="thumb"><a href="#"><img src="images/shop-product-img-4.jpg" alt=""></a><i class="fa fa-beer" aria-hidden="true"></i></div>
              <div class="text-col">
                <h3><a href="#">Vin Rosu</a></h3>
                <ul class="rating">
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star-half-full" aria-hidden="true"></i></a></li>
                </ul>
                <p>Vinul roșu este un tip de vin fabricat din soiuri de struguri de culoare închisă (negru).</p>
                <strong class="price">$130.00</strong> </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="product-col">
              <div class="thumb"><a href="#"><img src="images/shop-product-img-5.jpg" alt=""></a><i class="fa fa-beer" aria-hidden="true"></i></div>
              <div class="text-col">
                <h3><a href="#">Set de vinuri</a></h3>
                <ul class="rating">
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star-half-full" aria-hidden="true"></i></a></li>
                </ul>
                <p>Setul este format din vinuri:rosu,alb si roz                                </p>
                <strong class="price">$300.60</strong> </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="product-col">
              <div class="thumb"><a href="#"><img src="images/shop-product-img-6.jpg" alt=""></a><i class="fa fa-beer" aria-hidden="true"></i></div>
              <div class="text-col">
                <h3><a href="#">Vin alb</a></h3>
                <ul class="rating">
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-star-half-full" aria-hidden="true"></i></a></li>
                </ul>
                <p>Vinul alb este un vin care este fermentat fără contact cu coarja boabelor de poama.</p>
                <strong class="price">$25.00</strong> </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--ONLINE PRODUCT SECTION END-->

    <section class="blog-section">
      <div class="container">
        <h2>Ultimul nostru blog</h2>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <!--STICKY POST START-->
            <div class="post-box">
              <div class="frame"><a href="#"><img src="images/blog/blog-img-1.jpg" alt=""></a></div>
              <div class="text-box">
                <h2><a href="#"> Asconi</a></h2>
                <div class="post-meta">
                  <ul>
                    <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>03 Aug, 2017</a></li>
                    <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Dodon</a></li>
                    <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Alina</a></li>
                    <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i>204 Like</a></li>
                  </ul>
                </div>
                <p>Noi producem o varietate mare de vinuri inclusiv albe, roșii și rozeuri seci, vinuri de desert si vinuri tari. Majoritatea vinurilor roșii sunt maturate in baricuri de stejar de 225 litri. Folosim in mare parte doar stejar American sau Unguresc și mai puțin – stejar Frencez. Majoritatea vinurilor albe sunt în stilul proaspăt sau fresh cu excepția unor Chardonnay-uri si Riesing-uri care sunt fermentate sau maturate in stejar.</p>
                <a href="#" class="read-post">Citeste mai mult</a> </div>
            </div>
            <!--STICKY POST END-->
          </div>
          <div class="col-md-6 col-sm-6">
            <!--SLIDER POST START-->
            <div class="post-box">
              <div class="slider-frame">
                <div class="owl-carousel post-slider">
                  <div class="item"><img src="images/blog/blog-img-2.jpg" alt=""></div>
                  <div class="item"><img src="images/blog/blog-img-2.jpg" alt=""></div>
                </div>
              </div>
              <div class="text-box">
                <h2><a href="#"> Vinaria din Vale</a></h2>
                <div class="post-meta">
                  <ul>
                    <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>03 Aug, 2017</a></li>
                    <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Ion </a></li>
                    <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Rusu</a></li>
                    <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i>210 Likes</a></li>
                  </ul>
                </div>
                <p>Procesul de producere a vinurilor Vinăriei din Vale începe încă din podgorii. Plantațiile amplasate în regiunea Valul lui Traian se disting prin climă favorabilă și soluri fertile. Îngrijite cu cea mai mare atenție, podgoriile asigură, în primul rând, personalitatea viței de vie și calitatea strugurilor. </p>
                <a href="#" class="read-post">Citeste mai mult</a> </div>
            </div>
            <!--SLIDER POST END-->
          </div>
        </div>
      </div>
    </section>
    <section class="parallax-section">
      <div class="container">
        <div class="holder"><strong>O casă digitală de lux pentru un Brand care a șocat lumea</strong><a href="#" class="btn-style-1">Cumpara acum</a></div>
      </div>
    </section>
    <section class="testimonial-section">
      <div class="container">
        <h2>Consumatori fericiti</h2>
        <div class="inner-box">
          <div class="owl-controls post-slider">
            <div class="item">
              <p>A fost într-o seară călduroasă de vară. L-am pus în frigider cu 30 de minute înainte să-l beau. Foarte frumos. Mi-a adus aminte de Kendall Jacksons Pinot Noir.</p>
              <div class="btm-row">
                <div class="thumb"><img src="images/testimonial-img-2.jpg" alt=""></div>
                <strong class="name">Doina Plesca</strong> </div>
            </div>
            <div class="item">
              <p>A fost într-o seară călduroasă de vară. L-am pus în frigider cu 30 de minute înainte să-l beau. Foarte frumos. Mi-a adus aminte de Kendall Jacksons Pinot Noir.</p>
              <div class="btm-row">
                <div class="thumb"><img src="images/testimonial-img-2.jpg" alt=""></div>
                <strong class="name">Nastea Fusa</strong> </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!--FOOTER START-->
  <footer id="footer">
    <!--FOOTER SECTION 1 START-->
    <section class="footer-section-1">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="footer-box"> <strong class="footer-logo"><a href="#"><img src="images/footer-logo.png" alt="img"></a></strong>
              <p>Organizam excursii la vinarie unde puteți vedea tot procesul de vinificație. Puteți vedea cum strugurii recoltați se prelucrează și se preseaza, cum mustul este fermentati si transformat in vin și cum vinurile evolueaza după fermentare datorita unor procese ca și maturare in stejar sau în sticlă.</p>
              <a href="#" class="btn-style-1">Citeste mai mult despre asta</a> </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Alte servicii</h3>
              <div class="dental-services">
                <ul>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Micul dejun</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Masa de pranz</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Turism</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Excursie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Retele de socializare</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Bar & Wine Service</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Istorie</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Hotel</a></li>
                  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Producere</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Blogul recent</h3>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-1.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Apache</a></li>
                    <li><a href="#">02 Sep, 2017</a></li>
                  </ul>
                  <a href="#">Preturile la vin sunt greșite pentru a păcăli oamenii</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-2.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Tony Stark</a></li>
                    <li><a href="#">12 Sep, 2018</a></li>
                  </ul>
                  <a href="#">Singurul vin moldovenesc premiat cu medalie de aur la concursul Mundus Vini 2016.</a> </div>
              </div>
              <div class="recent-news">
                <div class="thumb"><a href="#"><img src="images/recent-img-3.jpg" alt="img"></a></div>
                <div class="text-box">
                  <ul>
                    <li><a href="#">Capitan America</a></li>
                    <li><a href="#">22 May, 2019</a></li>
                  </ul>
                  <a href="#">Aciditatea bine echilibrată și cele 14.8% alcool îi oferă premise pentru o învechire de durată la sticlă. </a> </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-box">
              <h3>Locatia</h3>
              <div class="footer-map">
                <div id="map_contact_2" class="map_canvas active"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--FOOTER SECTION 1 END-->

    <!--FOOTER SECTION 3 START-->
    <section class="footer-section-3">
      <div class="container"><strong class="copyright">Winecastle 2019, Toate drepturile sunt rezervate.</strong></div>
    </section>
    <!--FOOTER SECTION 3 END-->
  </footer>
  <!--FOOTER END-->
</div>
<!--JQUERY START-->
<script src="js/jquery.js"></script>
<!--BOOTSTRAP JS START-->
<script src="js/bootstrap.js"></script>
<!--Map Js-->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--OWL CAROUSEL JS START-->
<script src="js/owl.carousel.min.js"></script>
<!--CUSTOM JS START-->
<script src="js/custom.js"></script>
</body>
</html>
